﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebMVCApplication.BL;
using WebMVCApplication.Models;

namespace WebMVCApplication.Controllers
{
    public class TextEditorController : Controller
    {
        // GET: TextEditor
        public ActionResult Index()
        {
            return View();
        }

        TextEditorBL textEditorBL = new TextEditorBL();

        [HttpPost]
        public JsonResult Save(TextEditor textEditor)
        {
            try
            {
                textEditor = textEditorBL.Save(textEditor);
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Message = ex.Message;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(textEditor);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Get(TextEditor textEditor)
        {
            try
            {
                textEditor = textEditorBL.Get();
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Message = ex.Message;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(textEditor);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
    }
}