﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using WebMVCApplication.Models;

namespace WebMVCApplication.BL
{
    public class TextEditorBL
    {
        SqlConnection sqlCon = new SqlConnection(@"Data Source=localhost\SQLEXPRESS1;Initial Catalog=MVCWebApplicationDatabase;Integrated Security=True");
        SqlCommand sqlCom = null;

        public TextEditor Save(TextEditor textEditor)
        {
            try
            {
                sqlCon.Open();
                string sql = "INSERT INTO TextEditor (Text) VALUES('" + WebUtility.HtmlEncode(textEditor.Text) + "')";
                sqlCom = new SqlCommand(sql, sqlCon);
                sqlCom.ExecuteReader();
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Message = ex.Message;
            }
            finally
            {
                sqlCom.Dispose();
                sqlCon.Close();
            }
            return textEditor;
        }

        public TextEditor Get()
        {
            TextEditor textEditor = new TextEditor();

            try
            {
                sqlCon.Open();
                string sql = "SELECT TOP(1)* FROM TextEditor";
                sqlCom = new SqlCommand(sql, sqlCon);
                SqlDataReader reader = sqlCom.ExecuteReader();

                textEditor = new TextEditor();
                while (reader.Read())
                {
                    textEditor.EditorID = Convert.ToInt32(reader["EditorID"]);
                    textEditor.Text = WebUtility.HtmlDecode(reader["Text"].ToString());
                }
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Text = ex.Message;
            }
            finally
            {
                sqlCom.Dispose();
                sqlCon.Close();
            }
            return textEditor;
        }
    }
}